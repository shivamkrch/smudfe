-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Sep 06, 2018 at 01:56 AM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.2.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `smudfe`
--

-- --------------------------------------------------------

--
-- Table structure for table `smudfe_branch`
--

CREATE TABLE `smudfe_branch` (
  `branch id` int(50) NOT NULL,
  `branch code` text NOT NULL,
  `branch name` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `smudfe_branch`
--

INSERT INTO `smudfe_branch` (`branch id`, `branch code`, `branch name`) VALUES
(1, 'CSE', 'computer science and engineering'),
(2, 'ECE', 'electronics and communication engineering'),
(3, 'AEIE', 'applied electronics and instrumentation engineeering'),
(4, 'IT', 'information technology'),
(5, 'ME', 'mechanical engineering'),
(6, 'CE', 'civil engineering'),
(7, 'EE', 'electrical engineering');

-- --------------------------------------------------------

--
-- Table structure for table `smudfe_courses`
--

CREATE TABLE `smudfe_courses` (
  `course_id` int(11) NOT NULL,
  `courses` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `smudfe_courses`
--

INSERT INTO `smudfe_courses` (`course_id`, `courses`) VALUES
(1, 'c'),
(2, 'data structure'),
(3, 'java'),
(4, 'android'),
(6, 'Computer Organisation & Architecture'),
(7, 'Solid Mechanics'),
(8, 'Surveying'),
(9, 'web development'),
(10, 'basic electronics'),
(11, 'digital electronics');

-- --------------------------------------------------------

--
-- Table structure for table `smudfe_feedback`
--

CREATE TABLE `smudfe_feedback` (
  `id` int(11) NOT NULL,
  `name` varchar(40) NOT NULL,
  `email` varchar(40) NOT NULL,
  `comments` text NOT NULL,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `smudfe_feedback`
--

INSERT INTO `smudfe_feedback` (`id`, `name`, `email`, `comments`, `created`) VALUES
(1, 'Shivam Kumar', 'asdf@gm.co', 'shivam feedback', '2018-08-27 17:57:43');

-- --------------------------------------------------------

--
-- Table structure for table `smudfe_member`
--

CREATE TABLE `smudfe_member` (
  `id` int(11) NOT NULL,
  `name` varchar(40) NOT NULL,
  `college_roll` mediumint(11) NOT NULL,
  `univ_roll` bigint(20) NOT NULL,
  `branch` varchar(4) NOT NULL,
  `year` tinyint(4) NOT NULL,
  `email` varchar(50) NOT NULL,
  `password` varchar(32) NOT NULL,
  `gender` enum('male','female') NOT NULL,
  `mobile_no` bigint(11) NOT NULL,
  `courses` text,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `status` tinyint(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `smudfe_member`
--

INSERT INTO `smudfe_member` (`id`, `name`, `college_roll`, `univ_roll`, `branch`, `year`, `email`, `password`, `gender`, `mobile_no`, `courses`, `created`, `status`) VALUES
(4, 'Shivam Kumar', 130344, 12345677, 'CSE', 3, 'skc2@asd.cc', '25d55ad283aa400af464c76d713c07ad', 'male', 9876543210, '2,3,4,1', '2018-09-03 16:51:44', 3),
(29, 'Asdf ajdd', 1234555, 12345678, 'IT', 3, 'asdf@ad.cc', '71b3b26aaa319e0cdf6fdb8429c112b0', 'male', 999878898, '2,3,4,1', '2018-09-03 16:51:44', 3),
(44, 'shivendu kumar', 1603035, 12000116044, 'CSE', 3, 'shivu@asd.cc', 'd0970714757783e6cf17b26fb8e2298f', 'male', 9876543211, '2,3,4,1', '2018-09-03 16:51:44', 3),
(52, 'Shivam Kumar', 1603044, 12000116060, 'CSE', 3, 'shivamkmr.50397@gmail.com', 'e10adc3949ba59abbe56e057f20f883e', 'male', 8809249761, '2,3,4,1', '2018-09-03 16:51:44', 3);

-- --------------------------------------------------------

--
-- Table structure for table `smudfe_student`
--

CREATE TABLE `smudfe_student` (
  `id` int(11) NOT NULL,
  `name` varchar(40) NOT NULL,
  `gender` enum('Male','Female') NOT NULL,
  `college_roll` mediumint(50) NOT NULL,
  `univ_roll` bigint(50) NOT NULL,
  `branch` varchar(4) NOT NULL,
  `year` int(11) NOT NULL,
  `email` varchar(40) NOT NULL,
  `mobile_no` bigint(10) NOT NULL,
  `password` varchar(32) NOT NULL,
  `courses` text,
  `created` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `smudfe_student`
--

INSERT INTO `smudfe_student` (`id`, `name`, `gender`, `college_roll`, `univ_roll`, `branch`, `year`, `email`, `mobile_no`, `password`, `courses`, `created`, `status`) VALUES
(3, 'Shivam Kumar', 'Male', 123456, 8765432, 'IT', 3, 'skc@asd.cc', 8809345685, '25d55ad283aa400af464c76d713c07ad', '2,3,4,1', '2018-08-29 11:26:51', 1),
(13, 'Anshuman shikhar', 'Male', 1603034, 12000116115, 'CSE', 3, 'anshusonu@asd.cc', 7098416028, 'e10adc3949ba59abbe56e057f20f883e', '1,2,3,4', '2018-09-01 10:33:22', 1),
(14, 'Shivam Kumar', 'Male', 1603032, 12000116045, 'CSE', 3, 'shivamkmr.5039@gmail.com', 8809249765, 'd0970714757783e6cf17b26fb8e2298f', '1,2,3,4', '2018-09-01 19:26:35', 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `smudfe_branch`
--
ALTER TABLE `smudfe_branch`
  ADD PRIMARY KEY (`branch id`);

--
-- Indexes for table `smudfe_courses`
--
ALTER TABLE `smudfe_courses`
  ADD PRIMARY KEY (`course_id`);

--
-- Indexes for table `smudfe_feedback`
--
ALTER TABLE `smudfe_feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `smudfe_member`
--
ALTER TABLE `smudfe_member`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `college_roll` (`college_roll`),
  ADD UNIQUE KEY `univ_roll` (`univ_roll`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `mobile_no` (`mobile_no`);

--
-- Indexes for table `smudfe_student`
--
ALTER TABLE `smudfe_student`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `college_roll` (`college_roll`),
  ADD UNIQUE KEY `email` (`email`),
  ADD UNIQUE KEY `mobile_no` (`mobile_no`),
  ADD UNIQUE KEY `univ_roll` (`univ_roll`),
  ADD UNIQUE KEY `email_2` (`email`,`mobile_no`),
  ADD KEY `id` (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `smudfe_branch`
--
ALTER TABLE `smudfe_branch`
  MODIFY `branch id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `smudfe_courses`
--
ALTER TABLE `smudfe_courses`
  MODIFY `course_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `smudfe_feedback`
--
ALTER TABLE `smudfe_feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `smudfe_member`
--
ALTER TABLE `smudfe_member`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `smudfe_student`
--
ALTER TABLE `smudfe_student`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
