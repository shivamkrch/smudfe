<style>
    #nameFdb{text-transform: capitalize}
    .btn{}
</style>
<script>
    $('#feedbackForm').submit(function(e){
        var data = $('#feedbackForm').serializeArray();
        $.post('api/feedbackInsert.php', data,
        function(result){
            if(result.status === true){
                $('#feedbackForm').fadeOut();
                $('#fdbSucc').fadeIn();
            }else if(result.status === false){
                $('#fdbFail').fadeIn();
            }
        },'json');
        e.preventDefault();
    });
    $('title').text("Feedback | SMUDFE");
</script>
<div class="container p-sm-3 p-md-5 bg-light" id="feedback">
    <h1 style="color: #00D23F" class="pl-5 pt-1">Feedback</h1>
    <center>
        <form id="feedbackForm" method="get" class="p-4">
            <input type="text" name="nameFdb" class="form-control mx-auto my-3" id="nameFdb" placeholder="Name*" required>
            <input type="email" name="emailFdb" class="form-control mx-auto my-3" id="emailFdb" value="{{user.email}}"
                   placeholder="Email*" required>
            <textarea class="form-control mx-auto my-3" name="fdbFdb" id="fdbFdb" rows="5" placeholder="Feedback*" required></textarea>
            <input type="reset" class="btn btn-outline-secondary m-3"/>
            <input type="submit" id="fdbSubmitBtn" class="btn btn-outline-success m-3"/>
        </form>
        <h3 style="display: none" id="fdbSucc" class="p-5">Thanks for your feedback!</h3>
        <h3 style="color: #cc0000;display:none" id="fdbFail">Unable to submit feedback!</h3>
    </center>
</div>