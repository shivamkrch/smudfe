<div class="container p-sm-3 p-md-5 bg-light pb-5" id="profile" style="font-size: 18px;letter-spacing: 0.5px">
    <h1 style="color: #00D23F" class="p-3 pt-0">Profile</h1>
    <button type="button" class="btn btn-outline-info px-2 mr-3" data-toggle="modal" data-target="#changePwdModal"
            style="float: right" id="cpwdbtn">Change Password</button>
    <center>
    <table class="table table-borderless table-responsive-sm mt-3 ml-auto mr-auto" id="userDetails" style="font-size: 16px;">
        <tr><th class="pl-2">Name</th><td style="text-transform: capitalize">{{user.name}}</td></tr>
        <tr><th class="pl-2">Department</th><td>{{user.branch}}</td></tr>
        <tr><th class="pl-2">Year</th><td>{{year[user.year-1]}}</td></tr>
        <tr><th class="pl-2">College Roll</th><td>{{user.college_roll}}</td></tr>
        <tr><th class="pl-2">University Roll</th><td>{{user.univ_roll}}</td></tr>
        <tr><th class="pl-2">Email</th><td id="pfEmail">{{user.email}}<a title="Edit Email" id="edEmailIc"
                                            ng-click="editDet('#pfEmail','#edEmail')"><span class="material-icons ml-2"
                    style="font-size: 18px;cursor: pointer;color: #3888F4">edit</span></a></td>
                    <td id="edEmail" style="display: none">
                        <div class="input-group px-auto">
                            <input id="edEmailIn" class="form-control" type="email" value="{{user.email}}"
                                 placeholder="Enter new Email"  ng-keyup="checkDet('email','#edEmailIn')">
                            <div class="input-group-append">
                                <span class="material-icons mt-1 px-2" style="cursor: pointer" title="Cancel" ng-click="editDet('#edEmail','#pfEmail')">cancel</span>
                    <button type="button" class="btn btn-success btn-sm" id="edEmailBtn" ng-click="updateDet('email','#edEmailIn')">Save</button>
                            </div>
                        </div>
                    </td></tr>
        <tr><th class="pl-2">Mobile</th><td id="pfMob">{{user.mobile_no}}<a href="" title="Edit Mobile" id="edMobIc" 
                           ng-click="editDet('#pfMob','#edMob')"><span class="material-icons ml-2"
                    style="font-size: 18px;cursor: pointer;color: #3888F4">edit</span></a></td>
            <td  id="edMob" style="display: none">
                <div class="input-group px-auto">
                    <input type="number" id="edMobIn" class="form-control" value="{{user.mobile_no}}"
                           placeholder="Enter new Mobile No." ng-keyup="checkDet('mobile_no','#edMobIn')">
                <div class="input-group-append">
                <span class="material-icons mt-1 px-2" style="cursor: pointer" title="Cancel" ng-click="editDet('#edMob','#pfMob')">cancel</span>
                <button type="button" class="btn btn-success btn-sm" id="edMobBtn" ng-click="updateDet('mobile_no','#edMobIn')">Save</button>
                </div></div>
                </td></tr>
        <tr><th class="pl-2">Account Type</th><td ng-show="{{user.status<=2}}">Student</td>
            <td ng-show="{{user.status>=3}}">Member</td></tr>
    </table>
        <a href="javascript:void(0)" ng-show="{{user.status<=2 && user.year>=3}}" class="btn btn-outline-primary">
            Apply for Membership</a>
    </center>
    <div class="alert alert-warning m-5 p-3 alert-dismissible mx-auto" style="font-size: 14px;font-weight: bold">
            <button type="button" class="close p-3" data-dismiss="alert">&times;</button>
            To change University Roll, College Roll or any detail other than mobile and email, contact the SMUDFE Authority
                    with valid proofs (College ID, Registration Card etc.).
        </div>
        
    <h3 class="pt-5 pl-2" ng-show="{{user.status<=2}}">Courses enrolled: </h3>
    <h3 class="pt-5 pl-2" ng-show="{{user.status>=3}}">Courses teaching: </h3>
    <center>
    <p class="ml-3" ng-show="{{user.courses.length==0}}">None</p>
    <table ng-show="{{user.courses.length>=1}}" id="userCourses" class="table table-borderless table-hover table-responsive-sm mt-3">
        <tr ng-repeat="x in courses" ng-show="checkCourse(x.id)">
            <th  class="px-3 pl-5">{{x.course}}</th>
            <td class="px-auto"><a class="btn btn-danger" href="#!profile" ng-click="removeCourse(x.id)"
                                   title="Remove {{x.course}} from your courses">Remove</a></td>
        </tr>
    </table>
   <a href="#!courses" class="btn btn-outline-primary">Add Courses</a></center>
</div>
<?php
include_once './include/changePwdModal.php';
?>
<script>
    $('title').text(angular.element($('body')).scope().user.name + " | Profile | SMUDFE");
    $('#modalHeader').text("Change Password");
    $('#changePd').css('display','block');
</script>