<style>
    .carousel-indicators{position:absolute;right:0;bottom:3px;left:0;
                        z-index:15;display:-ms-flexbox;display:flex;-ms-flex-pack:center;
                        justify-content:center;padding-left:0;margin-right:15%;margin-left:15%;list-style:none}
    .carousel-indicators li{position:relative;-ms-flex:0 1 auto;flex:0 1 auto;
                            width:8px;height:8px;border-radius: 50%;
                            margin-right:5px;margin-left:5px;
                            text-indent:-999px;cursor:pointer;
                           background-color:rgba(255,255,255,.5)}
    .carousel-indicators li:hover{background-color: rgb(255,255,255)}
    .carousel-indicators .active,.carousel-indicators .active:hover{background-color:#00D23F;width: 9px;height: 9px}
    @media screen and (min-width: 701px){
        #slideshow{height: 330px;width: 700px}
    }
    @media screen and (max-width: 700px){
        #slideshow{height: 200px;}
    }
</style>
<center id="#main">
    <div id="slideshow" class="carousel slide shadow-sm mt-2" data-ride="carousel" data-interval="3000" style="overflow-y: hidden">

<!--   Indicators -->
  <ul class="carousel-indicators">
    <li data-target="#slideshow" data-slide-to="0" class="active"></li>
    <li data-target="#slideshow" data-slide-to="1"></li>
    <li data-target="#slideshow" data-slide-to="2"></li>
    <li data-target="#slideshow" data-slide-to="3"></li>
    <li data-target="#slideshow" data-slide-to="4"></li>
  </ul>

<!--   The slideshow -->
  <div class="carousel-inner">
    <div class="carousel-item active">
        <img src="images/img7.jpg" alt="img7" class="img-fluid">
    </div>
    <div class="carousel-item">
        <img src="images/img14.jpg" alt="img14" class="img-fluid">
    </div>
    <div class="carousel-item">
        <img src="images/img41.jpg" alt="img41" class="img-fluid">
    </div>
      <div class="carousel-item">
        <img src="images/img40.jpg" alt="img40" class="img-fluid">
    </div>
      <div class="carousel-item" class="img-fluid">
          <img src="images/img35.jpg" alt="img35" class="img-fluid">
    </div>
  </div>
</div></center>
<div class="container">
    <center class="p-4 pt-5 px-sm-2 px-5">
        <h2>Welcome to the official website of <b style="color: #00D23F;font-size: 40px;padding: 10px">SMUDFE</b></h2>
        <h2 style="color: #00D23F" class="p-3">Shreemati Urmila Devi Free Education</h2>
        <h5 style="color: #ff6600">SMUDFE is not an institute, it's a family.</h5>
    </center>
    <div class="media border p-3 bg-light mt-2">
        <img src="images/pkojha.jpg" alt="PK Ojha" class="mr-3 mt-3 rounded" style="width:80px;">
        <div class="media-body p-3">
            <h4>Prabhakar Kumar Ojha<small class="pl-3"><i><b>Founder</b></i></small></h4>
            <blockquote class="blockquote p-1" style="text-align: justify"><i>"It was started with an idea.
                    To train student to excel in academics by a student as a student only knows,
                    what are the mistakes they might do. "</i></blockquote>
        </div>
    </div>
</div>
<script>
    $('title').text("SMUDFE | Self dependency is the only dependency");
</script>