<div class="container p-sm-3 p-md-5 pt-5 bg-light" id="journey">
    <h1 style="color: #00D23F">Journey</h1>
    <p style="font-style: italic;font-size: 18px;text-align: justify" class="p-3"><b>Yesterday is history, tomorrow is a mystery, today is a gift of God,
            i.e why we call it present.</b></p>
            <p style="font-family: Roboto Slab, serif;text-align: justify;font-size: 17px;" class="px-md-5 px-sm-5 mb-3">SMUDFE is not an Institution, it is a family founded by Prabhakar Bhaiya in 2016 to achieve excellence in academic subjects in the field of engineering studies.  
In 2016, Prabhakar Bhaiya started with few junior students and after a few months  it is the best place to learn academic subjects free of cost under one roof
New heights of success were scaled year after year. The Institute achieved remarkable Landmark. The consistent and ideal team efforts of Prabhakar Bhaiya laid the path of glorious success, which led the institute to achieve success. SMUDFE is a symbol of a united family of few students of different branches headed by Prabhakar Bhaiya which has grown up to a large group & increasing everyday.
 With this, the institute is on its “Path to Success...” by writing its success story and adding more episodes of splendid works of our predecessor. 
</p>
<h3 class="pt-3">Future</h3>
    <p class="p-3" style="font-size: 18px;text-align: justify">The future is a mystery, We can’t tell where the next step of the journey will lead us,
        but one thing is for sure -
        <b style="font-style: italic;"> “We will keep teaching and learning, learning and teaching.”</b></p>
</div>
<script>
    $('title').text("Journey | SMUDFE");
</script>