<style>
    form>option{padding: 3px;}
</style>
<script src="js/register.js" type="text/javascript"></script>
<div class="container p-md-5 p-sm-3 bg-light" id="register">
    <h1 style="color: #00D23F" class="pl-3 pt-1">Register</h1>
    <form id="registerForm" class="p-3">
        <p> <a href="#!login" class="pl-4">Already registered?</a></p>
        <center>
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text fa fa-user" style="font-size:24px"></span>
            </div>
            <input type="text" name="regName" id="regName" class="form-control" placeholder="Name*" style="text-transform: capitalize" required>
        </div>
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text">Gender*</span>
            </div>
            <select class="form-control" id="genSel" name="genSel">
                <option value="">-Select-</option>
                <option ng-repeat="gen in gender" value="{{gen}}">{{gen}}</option>
            </select>
        </div>
        <input type="number" name="rollNo" id="rollNo" class="form-control mb-3" 
            placeholder="College Roll No.*" required>
        <input type="number" name="univRollNo" id="univRollNo" class="form-control mb-3"
            placeholder="University Roll No.*" required>
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text">Department*</span>
            </div>
            <select class="form-control" id="deptSel" name="deptSel" required>
                <option value="">-Select-</option>
                <option ng-repeat="dept in deptArr" value="{{dept}}">{{dept}}</option>
            </select>
        </div>
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text">Year*</span>
            </div>
            <select class="form-control" id="yearSel" name="yearSel" ng-model="y" required>
                <option value="">-Select-</option>
                <option ng-repeat="yr in year" value="{{yr[0]}}">{{yr}}</option>
            </select>
        </div>
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text">+91</span>
            </div>
            <input type="tel" name="regMob" id="regMob" minlength="10" maxlength="10" class="form-control" placeholder="Mobile*" required>
        </div>
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text"><b>@</b></span>
            </div>
            <input type="email" name="regEmail" id="regEmail" class="form-control" placeholder="Email*" required>
        </div>
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                    <span class="input-group-text fa fa-lock"style="font-size:24px"></span>
                </div>
            <input type="password" name="regPwd" id="regPwd" class="form-control mx-auto" minlength="6" placeholder="Password*" required>
            <div class="input-group-append">
                <button type="button" class="btn btn-primary" id="pwdBtn" title="Show Password" disabled><span class="fa fa-eye" id="eyeBtn"></span></button>
            </div>
        </div>
        <div class="input-group mb-3">
            <div class="input-group-prepend">
                <span class="input-group-text">Register as*</span>
            </div>
            <select class="form-control" id="typeSel" name="typeSel" required ng-model="mem">
                <option value="">-Select-</option>
                <option id="optStd" value="Student">Student</option>
                <option id="optMem" value="Member" ng-show="y>=3">Member</option>
            </select>
        </div>

        </center>
        <div class="container p-3" id="coursesSel" ng-switch="mem">
            <h5  ng-switch-when="Member">Select your courses:</h5>
            <div class="custom-control custom-checkbox" ng-repeat="crs in courses"  ng-switch-when="Member">
                <input type="checkbox" class="custom-control-input" id="course{{crs.id}}" name="course[]" value="{{crs.id}}">
                <label class="custom-control-label ml-2" for="course{{crs.id}}" style="text-transform: capitalize">{{crs.course}}</label>
            </div>
        </div>
        <center>
        <input type="reset" class="btn btn-outline-secondary mx-5 px-5"/>
        <input type="submit" value="Register" class="btn btn-outline-success px-5 mx-4"></center>
    </form>
    <center>
        <div  id="regSucc" style="display: none">
            <h3>You were registered successfully!</h3>
            <p><a href="#!login" class="px-2">Login</a> to continue.</p>
        </div>
        <h3 id="regFail" style="color: #cc0000;display: none">You were not registered!</h3>
    </center>
</div>
<script>
    $('title').text("Register | SMUDFE");
</script>