<script src="js/login.js" type="text/javascript"></script>
<div class="container p-md-5 p-sm-3 bg-light" id="login">
    <h1 style="color: #00D23F" class="pl-5 pt-3">Login</h1>
    <center>
        <div class="alert alert-danger" id="logFail" style="display: none">
            Invalid Credentials!
        </div>
        <p class="mt-4"><a href="#!register" class="px-2">Not registered yet? </a></p>
        <form id="loginForm" class="p-4 pt-2">
            <div class="input-group mb-3">
                <div class="input-group-prepend">
                    <span class="input-group-text fa fa-user" style="font-size:24px"></span>
                </div>
                <input type="email" name="loginEmailPh" id="loginEmailPh" class="form-control" placeholder="Email or Phone*" required>
            </div>
            <div class="input-group mb-3">
                 <div class="input-group-prepend">
                    <span class="input-group-text fa fa-lock"style="font-size:24px"></span>
                </div>
                <input type="password" name="loginPwd" id="loginPwd" class="form-control mx-auto" minlength="6" placeholder="Password*" required>
                <div class="input-group-append">
                    <button type="button" class="btn btn-primary" id="pwdBtn" title="Show Password" disabled=""><span class="fa fa-eye" id="eyeBtn"></span></button>
                </div>
            </div>
            <input type="submit" value="Login" class="btn btn-outline-success px-5" ng-click="loadUserData()" style="letter-spacing: 1.5px">
        </form>
        <button type="button" class="btn btn-outline-info px-2" data-toggle="modal" data-target="#changePwdModal"
                >Forgot Password?
        </button>
    </center>
</div>
<?php 
    include_once './include/changePwdModal.php';
?>
<script>
    $('title').text("Login | SMUDFE");
</script>