    $('#pwdBtn').click(function(e){
        if($('#eyeBtn').hasClass('fa-eye')){
            $('#regPwd').attr('type','text').focus();
            $('#pwdBtn').attr('title', 'Hide Password');
            $('#eyeBtn').removeClass('fa-eye').addClass('fa-eye-slash');
            
        }else{
            $('#regPwd').attr('type','password').focus();
            $('#pwdBtn').attr('title', 'Show Password');
            $('#eyeBtn').removeClass('fa-eye-slash').addClass('fa-eye');
        }
        e.preventDefault();
    });
    $('#typeSel').change(function(){
        if($('#typeSel').val()=="Member"){
            $('#coursesSel').fadeIn(200);
        }else{
            $('#coursesSel').fadeOut(200);
        }
    });
    $('#regPwd').keyup(function(){
        if($(this).val()!= ""){
            $('#pwdBtn').removeAttr('disabled');
        }
        else{
            $('#pwdBtn').attr('disabled','true');
        }
    });
    $('#registerForm').submit(function(e){
        var regData = $('#registerForm').serializeArray();
        $.post('api/registerInsert.php',regData,
        function(regInsRes){
            if(regInsRes.insert===true){
                $('#regSucc').fadeIn();
                $('#registerForm').fadeOut();
                $('#regFail').fadeOut();
            }else if(regInsRes.insert===false){
                $('#regFail').fadeIn();
            }
        },'json');
        e.preventDefault();
    });
    $('#rollNo').keyup(function(){
        var data = $(this).val();
        $.get("api/checkUnique.php",{elem:'#rollNo',data:data},
        function(result){
            if(result.name===null){
                $('#rollNo').tooltip('dispose');
                $('#rollNo').focus();
            }else{
                $('#rollNo').tooltip({title: 'Already registered as <span style="color:yellow">'+result.name+"</span> with this roll!",
                    trigger:'focus hover',html:true});
                $('#rollNo').tooltip('show');
            }
        },'json');
    });
    $('#univRollNo').keyup(function(){
        var data = $(this).val();
        $.get("api/checkUnique.php",{elem:'#univRollNo',data:data},
        function(result){
            if(result.name===null){
                $('#univRollNo').tooltip('dispose');
                $('#univRollNo').focus();
            }else{
                $('#univRollNo').tooltip({title: 'Already registered as <span style="color:yellow">'+result.name+"</span> with this roll!",
                    trigger:'focus hover',html:true});
                $('#univRollNo').tooltip('show');
            }
        },'json');
    });
    $('#regEmail').keyup(function(){
        var data = $(this).val();
        $.get("api/checkUnique.php",{elem:'#regEmail',data:data},
        function(result){
            if(result.name===null){
                $('#regEmail').tooltip('dispose');
                $('#regEmail').focus();
            }else{
                $('#regEmail').tooltip({title: 'Already registered as <span style="color:yellow">'+result.name+"</span> with this email!",
                    trigger:'focus hover',html:true});
                $('#regEmail').tooltip('show');
            }
        },'json');
    });
    $('#regMob').keyup(function(){
        var data = $(this).val();
        $.get("api/checkUnique.php",{elem:'#regMob',data:data},
        function(result){
            if(result.name===null){
                $('#regMob').tooltip('dispose');
                $('#regMob').focus();
            }else{
                $('#regMob').tooltip({title: 'Already registered as <span style="color:yellow">'+result.name+"</span> with this number!",
                    trigger:'focus hover',html:true});
                $('#regMob').tooltip('show');
            }
        },'json');
    });