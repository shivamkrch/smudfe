var smudfe_app = angular.module("smudfeApp",["ngRoute"]);
smudfe_app.config(function($routeProvider){
    $routeProvider.when("/",{
        templateUrl : "main.php"
    }).when("/posts",{
        templateUrl : "posts.php"
    }).when("/courses",{
        templateUrl : "courses.php"
    }).when("/journey",{
        templateUrl : "journey.php"
    }).when("/gallery",{
        templateUrl : "gallery.php"
    }).when("/feedback",{
        templateUrl : "feedback.php"
    }).when("/register",{
        templateUrl : "register.php"
    }).when("/login",{
        templateUrl : "login.php"
    }).when("/profile",{templateUrl : "profile.php"})
            .when("/editProfile",{templateUrl : "editProfile.php"});
});
smudfe_app.controller("smudfeCtrl", function($scope){
    $scope.deptArr = ["CSE","IT","ECE","EE","ME","CE","AEIE"];
    $scope.year = ["1st","2nd","3rd","4th"];
    $scope.types = ["Student","Member"];
    $scope.courses = [{id: 1,course:"C"},{id: 3,course:"JAVA"},{id:2,course:"Data Structures"},{id:4,course:"Android"},
        {id:6,course:"Computer Organisation & Architecture"},{id:7,course:"Solid Mechanics"},
        {id:8,course:"Surveying"},{id:9,course:"Web Development"},{id:10,course:"Basic Electronics"},
        {id:11,course:"Digital Electronics"}];
    $scope.gender = ["Male","Female"];
    $scope.loadUserData = function(){
        var data = $('#loginForm').serializeArray();
        $.post("../smudfe/api/loginCheck.php",data,
        function(res){
            if(res===null){
                $('#logFail').fadeIn();
            }else{
                $('#regLogin').fadeOut();
                $('#loginDd').fadeIn();
                $('#logFail').alert('close');
                $scope.user = res;
                if($scope.user.courses[0]==="")
                    $scope.user.courses=[];
                window.location = '#!profile';
            }
        },'json');
    };
    $scope.checkCourse = function(id){
        if($scope.user){
        for(var x=0; x<$scope.user.courses.length;x++){
            if($scope.user.courses[x] === id.toString())
                return true;
        }}
            return false;
    };
    $scope.addCourse = function(id){
        $.get("../smudfe/api/cookieCheck.php",{},
        function(res){
            if(res.status === false)
                window.location = "#!login";
            else{
                $.post("../smudfe/api/addCourse.php",{id:id,email:$scope.user.email},
                function(res){
                    $scope.user.courses = res;
                    window.location = "#!profile";
                    window.location = "#!courses";
                },'json');
            }
        },'json');
    };
    $scope.removeCourse = function(id){
        $.post("../smudfe/api/removeCourse.php",{id:id,email:$scope.user.email},
                function(res){
                    $scope.user.courses = res;
                    if($scope.user.courses[0]==="")
                        $scope.user.courses=[];
                    window.location = "#!courses";
                    window.location = "#!profile";
                },'json');
    };
    $scope.editDet = function(hide,show){
        $(hide).css('display','none');
        $(show).css('display','block');
    };
    $scope.checkDet = function(col,elem){
        var data = $(elem).val();
        $.get("../smudfe/api/checkEditDet.php",{data:data,col:col},
        function(res){
            if(res.email!==null){
                    $(elem).tooltip({title:"You or another user is already registered with this data.",trigger:"focus"});
                    $(elem).tooltip('show');
            }else{
                $(elem).tooltip('dispose');
            }
        },'json');
    };
    $scope.updateDet = function(col,elem){
        var data = $(elem).val();
        $.post("../smudfe/api/updateDetails.php",{col:col,data:data,email:$scope.user.email,type:$scope.user.status},
                function(res){
                    if(res.status===true){
                        $(elem).tooltip('dispose');
                        if(col === 'mobile_no'){
                            $scope.editDet('#edMob','#pfMob');
                            $scope.user.mobile_no = data;
                            window.location = "#!";
                            window.location = "#!profile";
                        }
                        if(col === 'email'){
                            $scope.editDet('#edEmail','#pfEmail');
                            $scope.user.email = data;
                            window.location = "#!";
                            window.location = "#!profile";
                        }
                        if(col === 'password'){
                            $scope.user.password = res.pwd;
                        }
                        
                    }else{
                        $(elem).tooltip({title:"Unable to update! Try again!"});
                        $(elem).tooltip('show');
                    }
                },'json');
    };
});