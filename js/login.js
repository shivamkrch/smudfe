$('#pwdBtn').click(function(){
        if($('#eyeBtn').hasClass('fa-eye')){
            $('#loginPwd').attr('type','text').focus();
            $('#pwdBtn').attr('title', 'Hide Password');
            $('#eyeBtn').removeClass('fa-eye').addClass('fa-eye-slash');
            
        }else{
            $('#loginPwd').attr('type','password').focus();
            $('#pwdBtn').attr('title', 'Show Password');
            $('#eyeBtn').removeClass('fa-eye-slash').addClass('fa-eye');
        }
    });
    $('#loginPwd').keyup(function(){
        if($(this).val()!= ""){
            $('#pwdBtn').removeAttr('disabled');
        }
        else{
            $('#pwdBtn').attr('disabled','true');
        }
    });