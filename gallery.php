<style>
    @media screen and (max-width: 600px){
        img-gall{height: 120px;}
    }
    
</style>
<div class="container p-2 mb-3 bg-light" style="overflow: hidden" id="gallery">
    
    <center><h1 class="p-3" style="color: #00D23F;letter-spacing: 1px">SMUDFE Gallery</h1>
    <?php
        $img_dir = './images/';
        $files = glob($img_dir. '*.jpg');
        if($files!==FALSE){
            $num_images = count($files);
        }
        for($i=1;$i<$num_images;$i++){
            echo '<img src="images/img'.$i.'.jpg" height="200" alt="img'.$i.'" class="m-2 img-gall shadow-sm">';
        }
    ?>
    </center>
</div>
<script>
    $('title').text("Gallery | SMUDFE");
</script>