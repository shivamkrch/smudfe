<?php
$email = $_POST['email'];
$name = ucwords($_POST['name']);
$otp = substr(str_shuffle('123456789'),0,6);
include_once './functions.php';
if(mail($email, "Email verification for password change request.",
        "<br>Hi <b>$name</b>,"
        . "<h4>There was a request to change password for your user account with SMUDFE.</h4>"
        . "<p>Enter the OTP <b>$otp</b> to verify your email and change password.</p>"
        . "<p>If not done by you, mail us at smudfedu@gmail.com</p>",
            implode("\r\n", $headers))){
        $otpSend['status'] = TRUE;
        $otpSend['otp'] = $otp;
    }else{
        $otpSend['status'] = FALSE;
    }
    echo json_encode($otpSend);
