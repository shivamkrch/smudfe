<?php
include_once './functions.php';
$query = "select * from smudfe_courses";
$result = mysqli_query($conn, $query);
$courses = array();
while ($row = mysqli_fetch_assoc($result)) {
    $courses[] = $row;
}
echo json_encode($courses);