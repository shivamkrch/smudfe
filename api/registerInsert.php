<?php
include_once './functions.php';
$name = $_POST['regName'];
$gender = $_POST['genSel'];
$collRoll = $_POST['rollNo'];
$univRoll = $_POST['univRollNo'];
$email = $_POST['regEmail'];
$mob = $_POST['regMob'];
$pwd = md5($_POST['regPwd']);
$dept = $_POST['deptSel'];
$year = $_POST['yearSel'];
$type = $_POST['typeSel'];
if($type=='Student'){
    $regQuery = "insert into smudfe_student (name,gender,college_roll,univ_roll,branch,year,email,mobile_no,password,status)"
            . " values('".$name."','".$gender."','".$collRoll."','".$univRoll."','".$dept."','".$year."','".$email."',"
            . "'".$mob."','".$pwd."',0)";
    
}elseif ($type=='Member') {
    $course = $_POST['course'];
    $courses = "";
    $courses = implode(",", $course);
    $regQuery = "insert into smudfe_member (name,gender,college_roll,univ_roll,branch,year,email,mobile_no,password,status,courses)"
            . " values('".$name."','".$gender."','".$collRoll."','".$univRoll."','".$dept."','".$year."','".$email."',"
            . "'".$mob."','".$pwd."',3,'".$courses."')";
}
$regIns = mysqli_query($conn, $regQuery);
echo mysqli_error($conn);
if(!$regIns){
    $regInsRes['insert'] = FALSE;
}else{
    $regInsRes['insert'] = TRUE;
    $extra="";
    if($type=="Member"){
        foreach ($course as $id) {
            $query3="select course from smudfe_courses where id='$id'";
            $crsRes = mysqli_query($conn, $query3);
            $res = mysqli_fetch_assoc($crsRes);
            $extra = $extra."<li>".$res['course']."</li>";
        }
        $extra = "<h3>Selected Courses</h3><ul>".$extra."</ul>";
    }
    if(mail($email, "You registered with SMUDFE",
        '<br><h3>Hi <b>'. ucwords($name).'</b>,</h3>'
        . '<h4>You were successfully registered with Shrimati Urmila Devi Free Education.</h4>'
        . '<p>You entered the following details: </p>'
        . '<table><tr><th>Name</th><td>'. ucwords($name).'</td><tr>'
            . '<tr><th>Gender</th><td>'.$gender.'</td></tr>'
            . '<tr><th>College Roll</th><td>'.$collRoll.'</td></tr>'
            . '<tr><th>University Roll</th><td>'.$univRoll.'</td></tr>'
            . '<tr><th>Department</th><td>'.$dept.'</td></tr>'
            . '<tr><th>Year</th><td>'.$year.'</td></tr>'
            . '<tr><th>Email</th><td>'.$email.'</td></tr>'
            . '<tr><th>Mobile</th><td>'.$mob.'</td></tr>'
            . '<tr><th>Account Type</th><td id="type">'.$type.'</td></tr></table>'
            . $extra."<br>".
            "<a href='https://smudfe.co.in/api/updateStatus.php?email=$email&type=$type'>Verify email</a>",
            implode("\r\n", $headers))){
        $regInsRes['mail'] = TRUE;
    }else{
        $regInsRes['mail'] = FALSE;
    }
}
echo json_encode($regInsRes);
mysqli_close($conn);
