<div class="container p-sm-3 p-md-5 pt-4 bg-light" id="courses">
    <h1 style="color: #00D23F" class="p-3 pt-1">Courses</h1>
    <center>
        <table class="table table-hover table-responsive-sm mt-5 p-3 ml-auto mr-auto mb-5" style="letter-spacing: 0.5px">
        <tr ng-repeat="crs in courses" class="border-bottom" style="vertical-align: central" ng-hide="checkCourse(crs.id)">
            <th class="pl-5 border-left" style="font-size: 17px;text-transform: capitalize">{{crs.course}}</th>
            <td class="px-5 border-right">
                <a style="color: white" class="btn btn-primary" title="Add {{crs.course}} to your courses" ng-click="addCourse(crs.id)">Add</a>
            </td>
        </tr>
    </table></center>
</div>
<script>
    $('title').text("Courses | SMUDFE");
</script>
