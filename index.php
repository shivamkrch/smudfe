<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
        <?php
            include_once './include/variables.php';
            echo $var;
        ?>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <style>
            body{font-family: Roboto,sans-serif}
            h1,h2,h3,h4,h5,h6{font-family: Didact Gothic, sans-serif;font-weight: bold;letter-spacing: 1px}
            th{text-transform: capitalize}
        </style>
    </head>
    <body ng-app="smudfeApp" data-spy="scroll" data-target=".navbar" data-offset="1000" ng-controller="smudfeCtrl">
        <?php
        echo $fbjssdk;
        include_once './include/header.php';
        ?>
        <div class="p-md-3" ng-view></div>
        <?php
        include_once './include/footer.php';
        ?>
        <script src="js/smudfe_angularjs.js" type="text/javascript"></script>
        <script>
            $.get("../smudfe/api/getCourseList.php",{},
            function(result){
                angular.element($('body')).scope().courses = result;
            },'json');
            var email,pwd;
            $.get("../smudfe/api/cookieCheck.php",{},
            function(result){
                if(result.status === true){
                    email = result.email;
                    pwd = result.pwd;
                    $.post("../smudfe/api/loginCheck.php",{loginEmailPh: email,loginPwd: pwd},
                    function(res){
                        $('#regLogin').fadeOut();
                        $('#loginDd').fadeIn();
                        window.location = "#!";
                        angular.element($('body')).scope().user = res;
                        if(angular.element($('body')).scope().user.courses[0]==="")
                            angular.element($('body')).scope().user.courses=[];
                    },'json');

                }
            },'json');
            
$('#logoutUser').click(function(){
    $.get("../smudfe/api/logoutUser.php",{},
    function(res){
                $('#regLogin').fadeIn();
                $('#loginDd').fadeOut();
                angular.element($('body')).scope().user = null;
                window.location = "#!";
                window.location = "#!login";
    },'json');
});
        </script>
    </body>
</html>
