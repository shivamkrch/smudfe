<style>
    footer{background-color: #2A2A2A}
</style>
<footer class="mt-5">
    <div class="container-fluid p-1 row">
        <div class="col-md-6 p-3 pt-5 pl-5">
            <?=$fbembed?>
        </div>
        <div class="col-md-6 p-5" style="color: whitesmoke">
            <h3>Contacts</h3>
            <ul class="fa-ul">
                <li><i class="fa fa-envelope mr-2" style="color: #cc0000"></i>smudfedu@gmail.com</li>
            </ul>
        </div>
    </div>
</footer>
