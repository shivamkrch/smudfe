<!-- The Modal -->
<style>
    .fade{
        transition-duration: 0.0s;
    }
</style>
<div class="modal fade" id="changePwdModal">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content shadow-lg">
        <!-- Modal Header -->
        <div class="modal-header shadow">
            <h2 class="modal-title ml-2" id="modalHeader">Change Password</h2>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body py-5 px-sm-1">
            <center>
                <div class="container" ng-show="{{user==null}}" id="verEm">
                    <input type="email" placeholder="Enter registered email" id="fgPwdEmail"
                           class="form-control m-3 mx-auto" required>
                    <input type="submit" value="Send OTP" class="btn btn-outline-success" id="sendOTPBtn" disabled>
                    
                <form id="verifyOTP" style="display: none">
                    <label for="otpInp" >A 6-digit OTP was sent to the email. </label>
                    <input type="number" max="999999" placeholder="Enter 6-digit OTP" id="otpInp"
                           class="form-control m-3 mx-auto" required>
                    <a href="javascript:void(0)" class="link" style="float: right" id="resendOTP">Resend OTP</a>
                </form>
            </div>
                <div class="container" style="display: none" id="changePd">
                    <form id="chPwdForm">
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text fa fa-lock"style="font-size:24px"></span>
                    </div>
                    <input type="password" name="changePwd" id="changePwd" class="form-control mx-auto" minlength="6"
                           placeholder="Enter new Password*" required>
                    <div class="input-group-append">
                        <button type="button" class="btn btn-primary" id="cpwdBtn" title="Show Password" disabled>
                            <span class="fa fa-eye" id="ceyeBtn"></span></button>
                    </div>
                </div>
                    <button type="submit" class="btn btn-outline-success" id="updPwdBtn" data-dismiss="modal"
                        ng-click="updateDet('password','#changePwd')" disabled>Update Password</button>
                </form>
                </div><i class="fa fa-spinner fa-pulse p-3" id="spinner1" style="display: none;font-size: 20px"></i></center>
        </div>
    </div>
  </div>
</div>
<script>
    $('#otpInp').keyup(function(){
        if($(this).val().length==6){
            if($(this).val()== angular.element($('body')).scope().otp){
                angular.element($('body')).scope().user='name';
                $('#verEm').fadeOut();
                $('#changePd').fadeIn();
                $(this).tooltip('dispose');
            }else{
                $(this).tooltip({title: "Invalid OTP."});
                $(this).tooltip('show');
            }
        }
    });
    $('#fgPwdEmail').keyup(function(){
        if($(this).val()!= ""){
            $('#sendOTPBtn').removeAttr('disabled');
        }
        else{
            $('#sendOTPBtn').attr('disabled','true');
        }
    });
    $('#changePwd').keyup(function(){
        if($(this).val()!= ""){
            $('#cpwdBtn').removeAttr('disabled');
            $('#updPwdBtn').removeAttr('disabled');
        }
        else{
            $('#cpwdBtn').attr('disabled','true');
            $('#updPwdBtn').attr('disabled','true');
        }
    });
    $('#cpwdBtn').click(function(){
        if($('#ceyeBtn').hasClass('fa-eye')){
            $('#changePwd').attr('type','text').focus();
            $('#cpwdBtn').attr('title', 'Hide Password');
            $('#ceyeBtn').removeClass('fa-eye').addClass('fa-eye-slash');
            
        }else{
            $('#changePwd').attr('type','password').focus();
            $('#cpwdBtn').attr('title', 'Show Password');
            $('#ceyeBtn').removeClass('fa-eye-slash').addClass('fa-eye');
        }
    });
    var name;
    $('#fgPwdEmail').keyup(function(){
        var data = $(this).val();
        $.get("api/checkUnique.php",{elem:'#regEmail',data:data},
        function(result){
            if(result.name===null){
                $('#sendOTPBtn').attr('disabled','true');
                $('#fgPwdEmail').tooltip({title: 'Invalid or unregistered email.',trigger:'focus hover',html:true});
                $('#fgPwdEmail').tooltip('show');
            }else{
                $('#sendOTPBtn').removeAttr('disabled');
                $('#fgPwdEmail').tooltip('dispose');
                name = result.name;
            }
        },'json');
    });
    $('#sendOTPBtn, #resendOTP').click(function(e){
        $('#spinner1').fadeIn();
        var data = $('#fgPwdEmail').val();
        $.post("../smudfe/api/sendOTP.php",{email:data,name:name},
        function(result){
            if(result.status===true){
                angular.element($('body')).scope().otp = result.otp;
                $('#sendOTPBtn').fadeOut();
                $('#fgPwdEmail').attr('readonly','true');
                $('#verifyOTP').fadeIn();
                $('#spinner1').fadeOut();
                $('#changePwdModal').modal({backdrop: 'static',keyboard: false});
            }else{
                $('#fgPwdEmail').tooltip({
                   title: "OTP not sent. Check your email, internet connection and try again." 
                });
                $('#fgPwdEmail').tooltip('show');
                $('#spinner1').fadeOut();
            }
        },'json');
        e.preventDefault();
    });
</script>
