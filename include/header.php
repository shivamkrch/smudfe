<link href="https://fonts.googleapis.com/css?family=Questrial|Didact+Gothic|Aldrich|Roboto|Lobster+Two|Roboto+Slab" rel="stylesheet">    
<style>
    #banner{padding: 10px;}
        #logo{padding: 0;margin-top: -48px;}
        #brand{color: #00D23F; font-weight: bold; font-family: Lobster Two, cursive;padding: 10px;}
        #tagline{font-family: Didact Gothic,sans-serif;text-align: center;font-weight: normal}
        #homelink:hover,#homelink:focus{text-decoration: none}
        .navbar{font-family: Aldrich, sans-serif}
        .nav-item{padding-left: 5px;padding-right: 5px;}
        @media only screen and (min-width: 700px) {
            #tagline{margin-left: 20rem;}
            
        }
        @media only screen and (max-width: 600px){
            #brand{font-size: 3.7rem}
        }
        #loginDd:hover{cursor: pointer}
    </style>
        <div class="container-fluid" id="banner">
            <ul class="list-inline" style="margin-bottom: 0">
                    <a href="#/!" id="homelink">
                    <li class="list-inline-item"><img src="https://scontent.fccu5-1.fna.fbcdn.net/v/t1.0-1/p200x200/21752260_306904736444999_5712986171448128795_n.jpg?_nc_cat=0&oh=c86d42427fdc458af6a3a7ebbca71304&oe=5C038338"
                         alt="SMUDFE_logo" width="80" height="80" id="logo" class="rounded-circle"/></li>
                    <li class="list-inline-item"><h1 class="display-3"  id="brand"
                                                     title="Shrimati Urmila Devi Free Education">SMUDFE</h1></li></a>
                    <li class="list-inline-item ml-5">
                        <h4 id="tagline">Self dependency is the only dependency.</h4>
                    </li>
                </ul>
        </div>
    <nav class="navbar navbar-expand-md sticky-top justify-content-md-center shadow-sm bg-dark navbar-dark py-1">
  <!-- Brand -->  <a class="navbar-brand ml-sm-5 slideanim1 x" id="home" href="#/!" title="Home" data-target="#main">HOME</a>
  <!-- Toggler/collapsibe Button -->
  <button class="navbar-toggler" title="Expand" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
                <span class="fa fa-chevron-down" id="navtoggle" style="color: whitesmoke;padding-bottom: 2px"></span>
  </button>
  <!-- Navbar links -->
  <div class="collapse navbar-collapse" id="collapsibleNavbar">
    <ul class="navbar-nav">
      <li class="nav-item navc">
        <a class="nav-link" href="#!posts" data-target="#posts">POSTS</a>
      </li>
      <li class="nav-item navc">
        <a class="nav-link" href="#!courses" data-target="#courses">COURSES</a>
      </li> 
      <li class="nav-item navc">
        <a class="nav-link" href="#!journey" data-target="#journey">JOURNEY</a>
      </li> 
      <li class="nav-item navc">
        <a class="nav-link" href="#!gallery" data-target="#gallery">GALLERY</a>
      </li> 
      <li class="nav-item navc">
        <a class="nav-link" href="#!feedback" data-target="#feedback">FEEDBACK</a>
      </li> 
      
  <!-- Login Dropdown -->
      <li class="nav-item dropdown" id="loginDd" style="display: none;text-transform: capitalize">
        <a class="nav-link dropdown-toggle" id="loginDrop" data-toggle="dropdown" style="color: whitesmoke">
              <span class="fa fa-user mr-2"></span>{{user.name}}
        </a>
        <div class="dropdown-menu" style="font-family: Roboto, sans-serif">
            <a class="dropdown-item navc" href="#!profile" data-target="#profile">Profile</a>
            <a class="dropdown-item navc" href="#!login" data-target="#logout" id="logoutUser">Logout</a>
        </div>
    </li>
    </ul>
      <ul class="navbar-nav navbar-right" id="regLogin">
          <li class="nav-item">
        <a class="nav-link navc" href="#!register" data-target="#register">REGISTER</a>
      </li> 
      <li class="nav-item">
        <a class="nav-link navc" href="#!login" data-target="#login">LOGIN</a>
      </li> 
      </ul>
  </div> 
</nav>

    <script>
    $('.navc').click(function(){
        $('.collapse').collapse('hide');
    });
    $('#home').click(function(){
        $('.collapse').collapse('hide');
    });
    
    </script>
